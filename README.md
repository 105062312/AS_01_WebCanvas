# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed



## Report 
* 實作說明
    js檔案裡有些微註記每個function是什麼功能的操作，在此再稍作說明。
    * "工具選擇"是當選擇了左側工具列內的物件時，將當下的state做變換，以免混淆而在canvas上造成混亂。
    剛開始預設為0，需點擊左側工具欄的工具才能在畫布上操作。
    * "滑鼠校正"、"畫線"、"監測滑鼠"是筆刷以及橡皮擦的功能，須注意的是engage function中會判斷當下的state，進而做出不同的操作。
    * "調整筆刷大小"除了能調整筆刷大小之外，其他工具如形狀工具、橡皮擦亦能藉此調整大小。為視窗中"Radius"的+或-的操作。
    * "調整顏色"能調整各種印到畫布上的圖形的顏色，能調整顏色的有筆刷、形狀工具、文字。
    * "清空畫布"將畫布初始化。注意畫布實際上畫布為透明底色，只是我在canvas加了背景以便顏色作區別。
    * "文字輸入"將輸入的文字印在滑鼠所點擊的位置。視窗中先點擊"T"字樣，並且在"Font size"填入字體大小、在"type here"填入要印出之內容，選擇字體之後(預設為微軟正黑體)，點擊畫布即可在點擊位置印出所打的文字。
    * "upload"將圖片上傳至畫布上。
    * "shape"三種形狀的畫法。
    * "redoundo"redo及undo的操作。
    * 下載圖片的功能寫在html裡，使用"<a>"標頭做操作(index.heml line 57)。
    * 鼠標圖示變換的功能也寫在heml裡，用onclick做簡單的鼠標變換。
* Basic component 
    * Brush and eraser
    <img src="./report/brush_n_eraser.png" width="960px" height="460px"></img>
    * Color selector
    <img src="./report/color.png" width="960px" height="460px"></img>
    * Simple menu
        * 簡易的左側工具列表以及右側的顏色選擇列表
    * Text input
    <img src="./report/text.png" width="960px" height="460px"></img>
    * Cursor icon
        * 選擇左側工具後，將滑鼠移動至畫布上時，鼠標圖示會改變
    * Refresh button
    <img src="./report/clear.png" width="960px" height="460px"></img>
* Advance tools
    * Circle, rectengle and triangle
    <img src="./report/shape.png" width="960px" height="460px"></img>
    * Un/Re-do button
    <img src="./report/unredo.png" width="960px" height="460px"></img>
    * Upload Image
    <img src="./report/up.png" width="960px" height="460px"></img>
    * Download Image
    <img src="./report/down.png" width="960px" height="460px"></img>
* Other useful widgets
    * 美化"上傳圖片"按鈕
        * type='file' 預設樣式影響排版，因此特別研究了如何將其樣式修改掉
    * 不過度縮小視窗頁面的情況下能大致維持排版(RWD)