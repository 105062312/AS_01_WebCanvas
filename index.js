var canvas = document.getElementById('canvas');
var context = canvas.getContext('2d');

var radius = 20;
var dragging = false;

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

context.lineWidth = radius*2;

//工具選擇
var tools = document.getElementsByClassName('btn');
var toolnum = 0;
for(var i=0, n=tools.length; i<n; i++){
    tools[i].addEventListener('click',  setTools);
}

function setTools(tl){
    var tool = tl.target;
    setTool(tool.id);
}

function setTool(toolID){
    if(toolID == "brush")
        toolnum = 1;
    else if(toolID == "eraser")
        toolnum = 2;
    else if(toolID == "word")
        toolnum = 3;
    else if(toolID == "triangle")
        toolnum = 4;
    else if(toolID == "circle")
        toolnum = 5;
    else if(toolID == "square")
        toolnum = 6;
   else if(toolID == "up")
        toolnum = 7;
}

//滑鼠校正
function getMousePos(canvas, e){
    var rect = canvas.getBoundingClientRect();
    return{
        x: (e.clientX - rect.left)/(rect.right - rect.left) * canvas.width,
        y: (e.clientY - rect.top) /(rect.bottom - rect.top) * canvas.height
    };
}

//畫線

var putPoint = function(e){
    let pos = getMousePos(canvas, e);
    if(toolnum==1)//筆刷
        context.globalCompositeOperation = 'source-over'; 
    else if(toolnum==2)//橡皮擦
        context.globalCompositeOperation = 'destination-out';
    if(dragging){
        context.lineTo(pos.x, pos.y);
        context.stroke();
        context.beginPath();
        context.arc(pos.x, pos.y, radius, 0, Math.PI*2);
        context.fill();
        context.beginPath();
        context.moveTo(pos.x, pos.y);
    }
}

var engage=function(e){
    if(toolnum==1 || toolnum==2){
        dragging = true;
        putPoint(e);
    }
    else if(toolnum==3){
        context.globalCompositeOperation = 'source-over'; 
        tex(e);
    }
    else if(toolnum==4){
        context.globalCompositeOperation = 'source-over'; 
        tri(e);
    }
    else if(toolnum==5){
        context.globalCompositeOperation = 'source-over'; 
        cir(e);
    }
    else if(toolnum==6){
        context.globalCompositeOperation = 'source-over'; 
        squ(e);
    }
    else if(toolnum==7){
        context.globalCompositeOperation = 'source-over'; 
        past(e);
    }
}

var disengage=function(){
    dragging = false;
    context.beginPath();
    _push();
}

//監測滑鼠
canvas.addEventListener('mousedown', engage);
canvas.addEventListener('mousemove', putPoint);
canvas.addEventListener('mouseup', disengage);

//調整筆刷大小
var minRad=5,
    maxRad=100,
    interval=5,
    radSpan = document.getElementById('radval'),
    decRad = document.getElementById('decrad'),
    incRad = document.getElementById('incrad');

var setRadius = function(newRadius){
    if(newRadius<minRad)
        newRadius = minRad;
    else if(newRadius>maxRad)
        newRadius = maxRad;
    radius = newRadius;
    context.lineWidth = radius*2;
    radSpan.innerHTML = radius;
}

decRad.addEventListener('click', function(){
    setRadius(radius-interval);
});

incRad.addEventListener('click', function(){
    setRadius(radius+interval);
});

//調整顏色
var swatches = document.getElementsByClassName('swatches');

for(var i=0, n=swatches.length; i<n; i++){
    swatches[i].addEventListener('click',  setSwatch);
}

function setSwatch(sw){
    var swatch = sw.target;
    setColor(swatch.style.backgroundColor);
}

function setColor(color){
    context.fillStyle = color;
    context.strokeStyle = color;
}

//清空畫布
var wipe = document.getElementById('wipe');
wipe.addEventListener('click', clear);

function clear(){
    wipe.addEventListener('click', clear);
    context.clearRect(0, 0, canvas.width, canvas.height);
}

//文字輸入
var text = document.getElementById('texting');
var fontsize = document.getElementById('fontsize');
var font = document.getElementById('font');

function tex(e){
    let pos = getMousePos(canvas, e);
    context.font =  fontsize.value+"px "+font.value;
    context.fillText(text.value,pos.x,pos.y)
}

//upload
var imgup = document.getElementById('up');
imgup.addEventListener('change', past, false);

function past(e){
    var loader = new FileReader();
    loader.onload = function(event){
        var img = new Image();
        img.onload = function(){
                        
            context.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    loader.readAsDataURL(e.target.files[0]);
}

//shape
function squ(e){
    let pos = getMousePos(canvas, e);
    context.beginPath();
    context.rect(pos.x, pos.y, radius, radius);
    context.stroke();
    context.fill();
}

function tri(e){
    let pos = getMousePos(canvas, e);
    var path=new Path2D();
    path.moveTo(pos.x+(radius/2)+50, pos.y+radius/2);
    path.lineTo(pos.x+(radius/2), pos.y+(radius/2)-50);
    path.lineTo(pos.x+(radius/2)-50, pos.y+radius/2);
    path.lineTo(pos.x+(radius/2)+50, pos.y+radius/2);
    path.lineTo(pos.x+(radius/2), pos.y+(radius/2)-50);
    context.stroke(path);
    context.fill(path);
}

function cir(e){
    let pos = getMousePos(canvas, e);
    context.beginPath();
    context.arc(pos.x, pos.y, radius, 0, Math.PI*2);
    context.stroke();
    context.fill();
}

//redoundo
var undo = document.getElementById('undo');
var redo = document.getElementById('redo');
var arr = new Array();
var step = -1;

undo.addEventListener('click', Undo);
redo.addEventListener('click', Redo);
function _push() {
    step++;
    if (step < arr.length) { arr.length = step; }
    arr.push(canvas.toDataURL());
}
function Undo() {
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        canvasPic.src = arr[step];
        clear();
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height); }
    }
}
function Redo() {
    if (step < arr.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = arr[step];
        clear();
        canvasPic.onload = function () { context.drawImage(canvasPic, 0, 0, canvas.width, canvas.height); }
    }
}